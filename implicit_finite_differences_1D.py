#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 16 12:39:12 2021

@author: fred
"""
import numpy as np
from scipy.optimize import fsolve 
import matplotlib.pyplot as plt
import numba as nb 

from non_canonical_1D import lattice1D
from finite_differences_1D import initializeSpace, xi, Lambda, averageH, gradE


@nb.jit
def getEqs(hnew, hi, him1, dx, dt):
    size = hi.size
    eqs = np.zeros_like(hi)
    for j in range(size):
        
        jm1 = (j-1) % size
        jp1 = (j+1) % size
        
        oneph2 = (1+hnew[j]**2)
        
        ddot = (him1[j] - 2 * hi[j] + hnew[j])/ dt**2
        lapl = - (hnew[jm1]+hnew[jp1] - 2*hnew[j])/dx**2 
        prefint = - hnew[j] / oneph2 
        int1 = - Lambda * hnew[j]**2 / (xi * oneph2)
        dot2 = (hnew[j]-him1[j])**2 / (4 * dt**2)
        grad2 = -(hnew[jp1]-hnew[jm1])**2 / (4 * dx**2) 
        
        eqs[j] = ( ddot + lapl + prefint * ( int1 + dot2 + grad2 ) )
        
        ####### eqs of the redefined field:
        # ddot = - (him1[j] - 2 * hi[j] + hnew[j])/ dt**2
        # lapl = (hnew[jm1]+hnew[jp1] - 2*hnew[j])/dx**2 
        # intt = - Lambda * np.tanh(hnew[j])**3 /( xi * np.cosh(hnew[j])**2)
        # eqs[j] = ddot + lapl + intt


    return eqs 
#%%
def timeStep(h, dx, dt):
    hi = np.array(h[-1])
    him1 = np.array(h[-2])
    hnewguess = np.copy(hi + (hi-him1))
    hnew = fsolve(getEqs, hnewguess, args=(hi, him1, dx, dt))
    h.append(hnew)

        

if __name__ == "__main__":
    
    tMax = 6e7
    dt = 1e3
    boxLength = 1e8
    size = 32
    freqs = [0.1,0.2,0.3]
    Nsteps = int(tMax/dt)
    t = np.linspace(0, tMax, Nsteps+2)
    dx = boxLength/size
    h = initializeSpace(size, boxLength, freqs, average=22, amplitude=1e-5)
    for _ in range(Nsteps):
        timeStep(h, dx, dt)
    
    #%%
    h = [lattice1D(hi, dx) for hi in h]
    ha = averageH(h)
    grad = gradE(h)
    #%%
    fig, (ax1, ax2) = plt.subplots(2, 1)
    ax1.plot(t, ha)
    ax1.set_ylim((-40,40))
    ax2.semilogy(t, grad)
    plt.show()