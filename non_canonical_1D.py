#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 14 00:43:11 2021

@author: fred
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import colors, cm
xi = 1e7
Lambda = 1e-3
expansion = 0
hubbleparam = 1 # more or less constant at the beginning of palatini

class lattice1D():
    def __init__(self, values, dx):
        self.values = list(values)
        self.dim = len(values)
        self.dx = dx
        
    def __getitem__(self, key:int): 
        # enforce periodic boundary conditions:
        key = key % self.dim        
        return self.values[key]

    def __add__(self, toadd):
        try:
            iter(toadd)
        except:
            return lattice1D([self[i] + toadd for i in range(self.dim)], self.dx)
        return lattice1D([self[i] + toadd[i] for i in range(self.dim)], self.dx)
    def __radd__(self, toadd):
        return self.__add__(toadd)

    def __sub__(self, tosub):
        try:
            iter(tosub)
        except:
            return lattice1D([self[i] - tosub for i in range(self.dim)], self.dx)
        return lattice1D([self[i] - tosub[i] for i in range(self.dim)], self.dx)
    def __rsub__(self, tosub):
        return self.__add__(tosub)

    def __mul__(self, factor):
        try:
            iter(factor)
        except:
            return lattice1D([self[i] * factor for i in range(self.dim)], self.dx)
        return lattice1D([self[i] * factor[i] for i in range(self.dim)], self.dx)
    def __rmul__(self, factor):
        return self.__mul__(factor)
    
    def __truediv__(self, divider):
        try:
            iter(divider)
        except:
            return lattice1D([self[i] / divider for i in range(self.dim)], self.dx)
        return lattice1D([self[i] / divider[i] for i in range(self.dim)], self.dx)
    def __rtruediv__(self, divider):
        return self.__truediv__(divider)

    def forwardGradient(self):
        return lattice1D([(self[i+1]-self[i])/self.dx for i in range(self.dim)], self.dx)
    def backwardGradient(self):
        return lattice1D([(self[i]-self[i-1])/self.dx for i in range(self.dim)], self.dx)
    def upwindGradient(self):
        alpha = 0.8
        return alpha * self.forwardGradient() + (1 - alpha) * self.backwardGradient()
    def centeredGradient(self):
        p = [-1, 0, 1]
        c = [-1/2, 0, 1/2]
        # p = [-2, -1, 0, 1, 2]
        # c = [1./12, -2./3, 0, 2./3, -1./12]
        # p = [-3, -2, -1, 0, 1, 2, 3]
        # c = [-1./60, 3./20, -3./4, 0, 3./4, -3./20, 1./60]
        # p = [-4, -3, -2, -1, 0, 1, 2, 3, 4]
        # c = [1./280, -4./105, 1./5, -4./5, 0, 4./5, -1./5, 4./105, -1/.280]
    
        l = [ sum((c[j] * self[i+p[j]] for j in range(len(c))))/self.dx for i in range(self.dim) ]
        
        return lattice1D(l, self.dx)

    def laplacian(self):
        # p = [-1, 0, 1]
        # c = [1, -2, 1]
        p = [-2, -1, 0, 1, 2]
        c = [-1./12, 4./3, -5./2, 4./3, -1./12]
        # p = [-4, -3, -2, -1, 0, 1, 2, 3, 4]
        # c = [-1./560, 8./315, -1./5, 8./5, -205./72, 8./5, -1./5, 8./315, -1./560]
        l = [ sum((c[j] * self[i+p[j]] for j in range(len(c))))/self.dx**2 for i in range(self.dim) ]
        return lattice1D(l, self.dx)
    
    
class Space():
    def __init__(self, h0, amplitude, boxLength, dim, freq):
        self.x = np.linspace(0, boxLength, dim)
        self.dx = self.x[-1] - self.x[-2]
        
        self.pert = np.zeros_like(self.x)
        for f in freq:
            self.pert += h0 * amplitude * np.sin(self.x * 2 * np.pi * f / boxLength) / f**0.5
        self.pert[-1] = 0
        self.pert = lattice1D(self.pert / len(freq), self.dx)
        
        
        self.h = self.pert + h0 
        
        # let's study the phase too
        self.phase = self.pert / h0 * 2*np.pi
        self.phasedot = lattice1D([0 for i in range(dim)], self.dx)
        
        self.hdot = lattice1D([0 for i in range(dim)], self.dx)
        
        self.dim = dim 
        self.freq = freq
        self.boxLength = boxLength 
        self.amplitude = amplitude 
        self.h0 = h0
        
        self.kernel = lattice1D([0 for i in range(dim)], self.dx)
        
        self.haverage = []
        self.Egradaverage = []
        self.laplacianaverage = []
        self.inttermaverage = []

    
    def calculateKernel(self):
        lapl = self.h.laplacian() 
        grad = self.h.centeredGradient() 
        grad2 = grad*grad
        h = self.h
        hdot = self.hdot
        h2 = h*h
        hdot2 = hdot*hdot
        intterm = xi* h / (1 + xi*h2) * ( hdot2 - grad2 - 4*xi * h2 / (1 + xi*h2))
        if expansion:
            hubblefriction = - 3 * hdot * hubbleparam
        else : 
            hubblefriction = 0
        return lapl + intterm + hubblefriction
    
    # consider that the phase is a perturbation
    def calculateKernelPhase(self):
        lapl = self.phase.laplacian()
        rho = self.h
        phasedot = self.phasedot
        rhodot = self.hdot
        gradphase = self.phase.centeredGradient()
        gradrho = self.h.centeredGradient()
        if expansion:
            hubblefriction = - 3 * phasedot * hubbleparam 
        else : 
            hubblefriction = 0
        return lapl - 2  / (rho*( rho + 2 * xi * rho*rho)) * (rhodot*phasedot - gradrho*gradphase) + hubblefriction
    
    
    # unitary gauge kernel:
    def calculateKernelChi(self):
        lapl = self.h.laplacian()
        chi = np.array( self.h.values)
        intterm = lattice1D(-4*xi**0.5 * np.tanh(xi**0.5 * chi)**3 / np.cosh(xi**0.5 * chi)**2, self.dx)
        return  lapl + intterm
    
    def updateKernel(self):
        # self.kernel = self.calculateKernelChi()
        # calculate for both the norm and the phase:
        self.kernel = [self.calculateKernel(), self.calculateKernelPhase()]
        # self.kernel = [self.calculateKernel()]

        
    def Euler(self, dt):
        # Euler just to make sure the Runge Kutta 3 below isn't going bananas
        self.hdot = self.hdot + self.kernel * dt 
        self.updateKernel() 
        self.h = self.h + self.hdot * dt
        
    def RK3(self, dt):
        A = [0., -0.8094877285808, -2.332279872, -1.7996225917882]
        B = [0.05673758865, 3.3135, 0.56148224863793, 0.3779776322563]
        deltaHdot = lattice1D([0 for i in range(self.dim)], self.dx)
        deltaH    = lattice1D([0 for i in range(self.dim)], self.dx)
        
        deltaPhasedot = lattice1D([0 for i in range(self.dim)], self.dx)
        deltaPhase    = lattice1D([0 for i in range(self.dim)], self.dx)
        for i in range(len(A)):
            self.updateKernel() 
            
            deltaHdot = A[i] * deltaHdot + dt * self.kernel[0]
            deltaH = A[i] * deltaH + dt * self.hdot 
            
            deltaPhasedot = A[i] * deltaPhasedot + dt * self.kernel[1]
            deltaPhase = A[i] * deltaPhase + dt * self.phasedot
            
            
            self.hdot =  self.hdot + B[i] * deltaHdot 
            self.h = self.h + B[i] * deltaH
            
            self.phasedot = self.phasedot + B[i] * deltaPhasedot
            self.phase = self.phase + B[i] * deltaPhase 
            
    def timeStep(self, dt):
        # go forward one time step and calculate observables as we go:
        self.RK3(dt) 
        hav = np.mean(self.h.values)
        self.haverage.append(hav)
        grad = self.h.forwardGradient() 
        grad2 = grad * grad
        self.Egradaverage.append(np.mean( 1 / (1 + xi * hav**2) * np.array(grad2.values) ))
#%%
if __name__ == "__main__":
    # initial value of the inflaton field:
    chi0 = 0.001
    h0 = 1/xi**0.5 * np.sinh(xi**0.5 * chi0) # undo the field redefinition
    # initial amplitude of perturbations:
    pert =  1e-5 # make it super small as to have some time before it explodes
    # range of perturbation momenta:
    # perturbation_freq = np.linspace(5, 10, 200)
    perturbation_freq =[5]
    # working in the same units as in cosmolattice. I'll send later a
    # draft detailing the said units
    box_length = 0.1
    # size of the box: 1D so feel free to bump it up 
    N = 32
    # the object I define above that contains everything
    s = Space(h0, pert, box_length, N, perturbation_freq)
    dt = 5e-5
    t = 0
    # initializing some list to store the intermediate states of the system
    ts = []
    states = []
    hdot, phases = [], []
    ffts, phaseffts = [], []
    
    #%%
    # main loop
    while t < 0.025:
        # evolve the system
        s.timeStep(dt)
        t += dt
        ts.append(t)
        # at each time step; save the norm h, as well as hdot, the phase
        # and the fourier transforms of the phase and the norm.
        states.append( s.h.values.copy() )
        ffts.append(np.abs(np.fft.rfft(s.h.values)))
        phaseffts.append(np.abs(np.fft.rfft(s.phase.values)))
        hdot.append( s.hdot.values.copy() )
        phases.append( s.phase.values.copy() )
    #%%
    # now plotting
    # fig, (ax1, ax2, ax3, ax4) = plt.subplots(4, 1, figsize=(6.2,6))
    
    # the gradient energy in the norm to make sure it goes up
    # ax1.semilogy(ts, s.Egradaverage)
    # ax1.set_ylabel(r"$\leftangle (\nabla h)^2(t) \rangle$")
    # ax1.set_xlim((0, ts[-1]))
    
    # # the average of the norm
    # ax2.plot(ts, s.haverage)
    # ax2.set_ylim((-2*h0,2*h0))
    # ax2.set_ylabel(r"$\leftangle h(t) \rangle$")
    # ax2.set_xlim((0, ts[-1]))

    
    
    #%%
    import matplotlib.colors as mcolors
    def make_colormap(seq):
        """Return a LinearSegmentedColormap
        seq: a sequence of floats and RGB-tuples. The floats should be increasing
        and in the interval (0,1).
        """
        seq = [(None,) * 3, 0.0] + list(seq) + [1.0, (None,) * 3]
        cdict = {'red': [], 'green': [], 'blue': []}
        for i, item in enumerate(seq):
            if isinstance(item, float):
                r1, g1, b1 = seq[i - 1]
                r2, g2, b2 = seq[i + 1]
                cdict['red'].append([item, r1, r2])
                cdict['green'].append([item, g1, g2])
                cdict['blue'].append([item, b1, b2])
        return mcolors.LinearSegmentedColormap('CustomMap', cdict)


    c = mcolors.ColorConverter().to_rgb
    rvb = make_colormap(
        [c('mediumblue'),  c('crimson')])#, 0.33, c('crimson'), c('crimson'), 0.66, c('crimson')])

    #%%
    plt.style.use('../thesis/plot_scripts/masterthesis.mplstyle')
    hdota = np.array(hdot)
    ha = np.array(states)
    phasesaorig = np.array(phases)
    
    phasesa = phasesaorig.copy()
    phaseshift = np.where(ha < 0)
    phasesa = np.abs(phasesa)
    
    fig, (ax3, ax4) = plt.subplots(2, 1, figsize=(6.2,4))
    yextent = ts[-1]/6
    leny = hdota.shape[1]
    dy = yextent / leny
    extent = [ts[0]-dt, ts[-1]+dt, -dy, yextent+dy]
    # ax3.imshow(hdota.transpose(), vmin=-3, vmax=3, extent=extent)
    # ax3.set_ylabel(r"$\dot{h}$ heatmap")
    ax3.imshow(ha.transpose(), vmin=-1*h0, vmax=1*h0, extent=extent, cmap=rvb, alpha=0.9)
    ax3.set_title(r"$\tilde{\rho}(\tilde{x},\tilde{t})$ heatmap")
    ax3.set_xlabel(r"time $\tilde{t}$")
    ax3.set_ylabel(r"position $\tilde{x}$")
    # the whole grid of hdot as an illustration of how the perturbations
    # in the norm evole
    
    ax3.set_yticks([])
    mpert = 5
    ax4.imshow(phasesa.transpose(), vmin=-mpert*pert, vmax=mpert*pert, extent=extent, cmap=rvb, alpha=0.9)
    ax4.set_ylabel(r"position $\tilde{x}$")
    ax4.set_xlabel(r"time $\tilde{t}$")
    ax4.set_title(r"$\theta(\tilde{x},\tilde{t})$ heatmap")
    ax4.set_yticks([])
    plt.tight_layout()
    # import matplotlib as mpl
    # mpl.use('pgf')
    # fig.savefig('../thesis/figures/1D_phase_evolution_heatmap.pgf')   
    #%%
    # and in a new figure, the power spectrum of the perturbations
    # in the phase. We see that it doesn't evolve much with time.
    """
    plt.figure()
    cmap = cm.get_cmap('magma')
    norm = colors.Normalize(vmin=ts[0], vmax=ts[-1])
    every = 250
    for i in range(len(ts)//every):
        j = every*i
        plt.plot(phaseffts[j][2:]**2, label=f"t={ts[j]:.04f}", color=cmap(norm(ts[j])))
    plt.legend()
    #"""
    
