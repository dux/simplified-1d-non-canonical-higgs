#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 14 21:01:13 2021

@author: fred
"""
import numpy as np
import matplotlib.pyplot as plt
# import numba as nb
xi = 1e7 
Lambda = 1e-3

from non_canonical_1D import lattice1D


def hNextHomogeneous(h, dt):
    hi = h[-1]
    him1 = h[-2]
    sqrt =( (xi * (1 + hi**2) + (dt**2 * Lambda - xi) *hi**4 - xi * hi**6  \
           + 2 * xi * him1 * hi * (1 + hi**2)**2) / (dt**4 * xi * (1+hi**2)**3) )**0.5
    
    new = him1 - ( 2 * (1 + hi**2) * (-1 + dt**2 * sqrt ) ) / hi
    
    h.append(new)


def evolveHomegeneous():
    dt = 1e4
    h = [22, 22]
    for i in range(10000):
        hNextHomogeneous(h, dt)
    
    return h

def hNextFull(h, dt, dx):
    # here, h will be a list of lists
    
    # space at present time
    hi = h[-1]
    # space at previous time step
    him1 = h[-2]
    # space at next time step:
    hnext = []
    # size of space:
    size = len(hi)
    # loop trough each spatial position:
    for j in range(size):
        # periodic boundary:
        jm1 = (j-1) % size
        jp1 = (j+1) % size
        # see mathematica file for the origin of the following expression.
        prefactor = -1./hi[j] * (1 + hi[j]**2)
        
        p1 = -2 - him1[j]*hi[j] / (1 + hi[j]**2)
        
        sqrtprefactor = 1. / ( dx * xi**0.5 * (1 + hi[j]**2)**(1.5) )
        
        sqrt1 = 4 * dt**2 * dx**2 * Lambda * hi[j]**4 
        sqrt2 = 4 * dx**2 * xi * (1 + 2*him1[j]*hi[j] - hi[j]**2) * (1+hi[j]**2)**2
        prefsqrt3 = dt**2 * xi * hi[j] * (1 + hi[j]**2)
        sqrt3 =      hi[jm1]**2 * hi[j] \
               + 8 * ( hi[j] + hi[j]**3) \
               - 4 * ( 1 + hi[j]**2 ) * hi[jp1] \
               +     hi[j] * hi[jp1]**2 \
               - 2 * hi[jm1] * ( 2 + hi[j] * ( 2 * hi[j] + hi[jp1]) )
        sqrt = (sqrt1+sqrt2+prefsqrt3*sqrt3)**0.5 
        hnext.append( prefactor * (p1 + sqrtprefactor*sqrt) )
        
    h.append(hnext)

def initializeSpace(size, boxLength, freqs, average, amplitude):
    x = np.linspace(0, boxLength, size)
    perturbation = np.zeros_like(x)
    for freq in freqs:
        perturbation += np.sin( 2*np.pi * x * freq / boxLength)
    perturbation[-1] = 0
    h0 = list(amplitude * perturbation + average)
    return [h0.copy(), h0.copy()]


def evolveFull(tMax, dt, boxLength, freqs):

    dx = boxLength / size 
    
    h = initializeSpace(size, boxLength, freqs, average=22, amplitude=1e-8)
    for i in range(int(tMax/dt)):
        hNextFull(h, dt, dx)
    
    h = [lattice1D(hi, dx) for hi in h]
    return h

def averageH(h):
    return [np.mean(hi.values) for hi in h]
def stdH(h):
    return [np.std(hi.values) for hi in h]
def gradE(h):
    E = []
    for hi in h:
        grad = hi.forwardGradient().values 
        E.append((1/(1 + np.power(hi.values,2)) * np.power(grad, 2)).sum() / (2 * xi))
    return E
#%%
if __name__ ==  "__main__":
    
    tMax = 6e7
    dt = 5e3
    boxLength = 1e8
    size = 16
    freqs = [0.1, 0.5, 1]
    t = np.linspace(0, tMax, int(tMax/dt)+2)
    
    h = evolveFull(tMax, dt, boxLength, freqs)
    #%%
    ha = averageH(h)
    grad = gradE(h)
    
    fig, (ax1, ax2) = plt.subplots(2, 1)
    ax1.plot(t, ha)
    ax2.semilogy(t, grad)
    plt.show()
